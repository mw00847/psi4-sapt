#uses pickle to bring in the data from psi4 

#plots the data with matplotlib 

import pickle 
import matplotlib.pyplot as plt
import numpy as np

pickle_in = open("dict.pickle","rb")

SAEG_disp = pickle.load(pickle_in)
SAEG_elst = pickle.load(pickle_in)
SAEG_exch = pickle.load(pickle_in)
SAEG_ind = pickle.load(pickle_in)
SAEG_tot = pickle.load(pickle_in)

PPANPG_disp = pickle.load(pickle_in)
PPANPG_elst = pickle.load(pickle_in)
PPANPG_exch = pickle.load(pickle_in)
PPANPG_ind = pickle.load(pickle_in)
PPANPG_tot = pickle.load(pickle_in)

PPAEG_disp = pickle.load(pickle_in)
PPAEG_elst = pickle.load(pickle_in)
PPAEG_exch = pickle.load(pickle_in)
PPAEG_ind = pickle.load(pickle_in)
PPAEG_tot = pickle.load(pickle_in)

MPANPG_disp = pickle.load(pickle_in)
MPANPG_elst = pickle.load(pickle_in)
MPANPG_exch = pickle.load(pickle_in)
MPANPG_ind = pickle.load(pickle_in)
MPANPG_tot = pickle.load(pickle_in)

MPADEG_disp = pickle.load(pickle_in)
MPADEG_elst = pickle.load(pickle_in)
MPADEG_exch = pickle.load(pickle_in)
MPADEG_ind = pickle.load(pickle_in)
MPADEG_tot = pickle.load(pickle_in)

AAEG_disp = pickle.load(pickle_in)
AAEG_elst = pickle.load(pickle_in)
AAEG_exch = pickle.load(pickle_in)
AAEG_ind = pickle.load(pickle_in)
AAEG_tot = pickle.load(pickle_in)

#plots each of the non covalent energies together

#dispersion

x = np.array([0,1,2,3,4,5])
label = ['AA_EG','MPA_DEG','MPA_NPG','PPA_EG','PPA_NPG','SA_EG']
a = (AAEG_disp,MPADEG_disp,MPANPG_disp,PPAEG_disp,PPANPG_disp,SAEG_disp)

plt.xticks(x, label)
plt.bar(x,a)
plt.title("Dispersion")
plt.ylabel("Eh")
#plt.show()

#electrostatic

b = (AAEG_elst,MPADEG_elst,MPANPG_elst,PPAEG_elst,PPANPG_elst,SAEG_elst)
plt.xticks(x, label)
plt.bar(x,b)
plt.title("Electrostatic")
plt.ylabel("Eh")
#plt.show()

#exchange

c = (AAEG_exch,MPADEG_exch,MPANPG_exch,PPAEG_exch,PPANPG_exch,SAEG_exch)
plt.xticks(x, label)
plt.bar(x,c)
plt.title("Exchange")
plt.ylabel("Eh")
#plt.show()

#induction 

d = (AAEG_ind,MPADEG_ind,MPANPG_ind,PPAEG_ind,PPANPG_ind,SAEG_ind)
plt.xticks(x, label)
plt.bar(x,d)
plt.title("Induction")
plt.ylabel("Eh")
#plt.show()

#total 
e = (AAEG_tot,MPADEG_tot,MPANPG_tot,PPAEG_tot,PPANPG_tot,SAEG_tot)
plt.xticks(x, label)
plt.bar(x,e)
plt.ylabel("Eh")
plt.title("Total")


plt.show()





 